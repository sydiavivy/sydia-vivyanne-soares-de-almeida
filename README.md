## Sistema de autenticação

O sistema deverá ter uma tela de login e uma tela principal.

**TELA LOGIN**

A tela de login deverá ser composta de dois campos:

-   Login: (nome de usuário)
-   Senha: (8 caracteres contendo pelo menos uma ocorrência de letra maiúscula e números)

**Regra de autenticação:**

A autenticação do sistema deverá permitir dois tipos de perfil de usuários:  

**_usuario.admin_**

Ao efetuar login, o sistema deverá ser redirecionado para a tela principal, onde terá a seguinte mensagem:
**"Eu sou usuário administrador do sistema."**

**_usuario.comum_**

Ao efetuar login, o sistema deverá ser redirecionado para a tela principal, onde terá a seguinte mensagem:
**"Eu sou usuário comum do sistema."**

**TELA PRINCIPAL**

Deverá ter apenas o nome do usuário e a mensagem de acordo com o tipo de usuário logado.

_**Exemplo:**_  

### Meu nome é José da silva. 
## Eu sou usuário administrador do sistema.

 1. O sistema deverá ser desenvolvido para plataforma web. 
 2. O candidato poderá ficar livre para usar qualquer tecnologia. 
 3. Em relação a dados o candidato poderá ficar a vontade para usar qualquer tipo de armazenamento, podendo ser um banco de dados físico ou embarcado, arquivo, memória entre outros.

